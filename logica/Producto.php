<?php
require_once 'persistencia/ProductoDAO.php';
require_once 'persistencia/Conexion.php';
class Producto{
    private $id;
    private $nombre;
    private $precio;
    private $categoria;
    private $conexion;
    private $productoDAO;
    
    
    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }

    public function getConexion()
    {
        return $this->conexion;
    }

    public function getProductoDAO()
    {
        return $this->productoDAO;
    }

    public function Producto($id="", $nombre="", $precio="", $categoria=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> categoria = $categoria;
        $this -> conexion = new Conexion();
        $this -> productoDAO = new ProductoDAO($id, $nombre, $precio, $categoria);
    }
    
    public function crear() {
        
        $this -> conexion ->Abrir();
        $this -> conexion ->ejecutar($this -> productoDAO -> crear());
        $this -> conexion ->cerrar();
    }
    
    public function consultarTodos($atributo, $direccion, $filas, $pag){
        $this -> conexion -> abrir();
      //  echo $this -> productoDAO -> consultarTodos($atributo, $direcscion, $filas, $pag);
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodos($atributo, $direccion, $filas, $pag));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $categoria = new Categoria($resultado[3]);
            $categoria -> consultar();
            array_push($productos, new Producto($resultado[0], $resultado[1], $resultado[2], $categoria));
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarTotalFilas(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTotalFilas());
        return $this -> conexion -> extraer()[0];
    }
    
    
}
