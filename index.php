<?php 
require_once "logica/Categoria.php";
require_once "logica/Producto.php";

$pid="";
if( isset ($_GET["pid"])){
    
    $pid = base64_decode($_GET["pid"]);
}
?>
<!doctype html>
<!-- estas lineas son para bootstrap y eso -->
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
<title>Sports DC</title>

<link rel="icon" type="image/png" href="img/logos.png" />
</head>
<body>
<?php
if ($pid != "" ) {
    include $pid;
} else {
    include "presentacion/inicio.php";
}

?>
<div class="container">
		<div class="row mt-3">
			<div class="col-4">
				<img src="img/adidas.png" width="50%">

			</div>
			<div class="col-4">
				<img src="img/nike.png" width="50%">

			</div>
			<div class="col-4">
				<img src="img/under.png" width="50%">


			</div>

		</div>
		<div class="row mt-3">
			<div class="col-4">
				<img src="img/reebook.png" width="50%">

			</div>
			<div class="col-4">
				<img src="img/puma.png" width="50%">

			</div>
			<div class="col-4">
				<img src="img/adidas 2.png" width="50%">


			</div>
			<div class="row mt-80">
				<div class="row">
					<div class="col text-center text-muted">
					Criis R &copy; <?php echo date("Y") ?>	
				</div>
				</div>
			</div>

		</div>
	</div>
</body>
</html>