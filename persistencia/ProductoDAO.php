<?php
class ProductoDAO
{
    private $id;
    private $nombre;
    private $precio;
    private $categoria;
   
    public function ProductoDAO($id="", $nombre="", $precio="", $categoria=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> categoria = $categoria;
    }
    
    public function crear(){
        return "insert into producto (nombre, precio, categoria_idcategoria)
                values (
                '" . $this -> nombre . "',
                '" . $this -> precio . "',
                '" . $this -> categoria . "'
                )";
    }
    
    public function consultarTodos($atributo, $direccion, $filas, $pag){
        return "select idproducto, nombre, precio, categoria_idcategoria
                from producto " .
                (($atributo != "" && $direccion != "")?"order by " . $atributo . " " . $direccion:"") .
                " limit " . (($pag-1)*$filas) . ", " . $filas;
    }
    
    public function consultarTotalFilas(){
        return "select count(idproducto)
                from producto";
    }
}

