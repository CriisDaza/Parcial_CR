
<?php
include "presentacion/menu.php";
$veces = 1;

if (isset($_GET["veces"])) {
    $veces = $_GET["veces"];
}

if(isset($_POST["crear"])){
    for ($i = 1; $i <= $veces; $i ++) {
        
        $producto = new Producto("", $_POST["nombre" . $i ], $_POST["precio" . $i ], $_POST["categoria" . $i]);
        $producto -> crear();
    }
}
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Crear Producto</h5>
				<div class="card-body">
					<div class="row">
						<div class="col-2">
							Resultados:<select class="form-select" id="veces">
								<option value="1" <?php if($veces==1) echo "selected"?>>1</option>
								<option value="2" <?php if($veces==2) echo "selected"?>>2</option>
								<option value="3" <?php if($veces==3) echo "selected"?>>3</option>
								<option value="4" <?php if($veces==4) echo "selected"?>>4</option>
								<option value="5" <?php if($veces==5) echo "selected"?>>5</option>
							</select>
						</div>
					</div>
				</div>
				<form></form>
			</div>
		</div>
	</div>

	<div class="row mt-3">
		<div class=" col-sm-1 col-md-3"></div>
		<div class="col-12 col-sm-10 col-md-6">
			<div class="card">
				<div class="card-body">

					<form action="index.php?pid=<?php echo base64_encode("presentacion/producto/crearproductos.php") ."&veces=". $veces ?>"
						method="post">
					
                    <?php
                     for ($i = 1; $i <= $veces; $i ++) {
                      ?>
                      
					<h5 class="card-header mt-3">Producto <?php echo " " . "$i" ?></h5>
					
                     <?php if (isset($_POST["crear"])) {  ?>
					<div class="alert alert-success alert-dismissible fade show"
							role="alert">
							Datos ingresados correctamente.
							<button type="button" class="btn-close" data-bs-dismiss="alert"
								aria-label="Close"></button>
						</div>
						<?php  } ?>
						<div class="mb-3">
							<label class="form-label mt-2">Nombre Producto <?php echo " " . "$i" ?></label> <input type="text"
								class="form-control" <?php echo "name='nombre"  . $i ."'" ?> required="required">
						</div>
						<div class="mb-3">
							<label class="form-label">Precio Producto <?php echo " " . "$i" ?></label> <input type="number"
								class="form-control" <?php echo "name='precio"  . $i ."'" ?> required="required">
						</div>
						
						<label class="form-label">Tipo Producto <?php echo " " . "$i" ?></label> <select
							class="form-select" <?php echo "name='categoria" . $i ."'" ?>>
            <?php
            
            $categoria = new Categoria();
            $categorias = $categoria->consultarTodos();
            foreach ($categorias as $categoriaActual) {
                echo "<option value='" . $categoriaActual->getId() . "'>" . $categoriaActual->getNombre() . "</option>";
            }
               ?>
			</select>
			<?php  } ?>
			<div class="d-grid">
							<button type="submit" name="crear" class="btn btn-primary">Crear</button>
						</div>
			
				</form>
				</div>

				
			</div>
		</div>

	</div>
</div>

<script>
$("#veces").change(function() {
	var veces = $("#veces").val(); 
	var url = "index.php?pid=<?php echo base64_encode("presentacion/producto/crearproductos.php") ?>&veces=" + veces;
	location.replace(url);  	
});
</script>
